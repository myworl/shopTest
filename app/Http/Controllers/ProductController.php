<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use PHPUnit\Exception;


class ProductController extends Controller
{
    public function productAdd(ProductRequest $request)
    {
        $product_count = Product::where('categories_id',$request->get('categories_id'))->get()->count();
        if($product_count < 10){
            $product_add = Product::create([
                'name' => $request->get('name'),
                'categories_id' => $request->get('categories_id'),
            ]);
            return new ProductResource($product_add);
        }
        return response()->json(['error'=>'В 1 категории не может быть больше 10 товаров']);
    }

    public function productEdit(ProductRequest $request, $id)
    {
        $update_data = [];

        if (null !== $request->get('name'))
        {
            $update_data['name'] = $request->get('name');
        }

        if (null !== $request->get('order'))
        {
            $update_data['order'] = $request->get('order');
        }

        if (null !== $request->get('isActive'))
        {
            $update_data['isActive'] = $request->get('isActive');
        }

        if (!$product = Product::find($id))
        {
            return response()->json(['error'=>'нет такой запись в БД']);
        }

        try {
            $product->update($update_data);
        }catch(Exception $e){
            return response()->json(['error'=>'Ошибка добавление записи']);
        }

        return new ProductResource($product);
    }

    public function productDelete($id)
    {
        $product = Product::find($id);
        $product->isDelete=1;
        $product->save();
        return new ProductResource($product);
    }
}
