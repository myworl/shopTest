<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\Product;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ProductListController extends Controller
{
    public function productsGetAll(Request $request){
        $validator = Validator::make($request->all(), [
            'product_name' => 'nullable|string|max:100',
            'category_id' => 'nullable|integer',
            'category_name' => 'nullable|string|max:100',
            'product_price_up_to' => 'nullable|integer|min:0',
            'product_price_from' => 'nullable|integer|min:0',
            'prodict_is_active' => 'nullable|integer|min:0|max:1',
            'prodict_no_delete' => 'nullable|integer|min:0|max:1',
        ]);

        $errors = $validator->errors();

        if($errors->all()){
            response()->json([
                "error" =>  $errors->all(),
            ]);
        }

        $data = $validator->validated();

        $query = Category::query();
        if (Arr::has($data, 'category_id')) {
            $query->where('id', $data['category_id']);
        }
        if (Arr::has($data, 'category_name')) {
            $query->where('name', 'like', '%'.$data['category_name'].'%');
        }
        $category_result = array();
        foreach ($query->get() as $items){
            $category_result[] = $items['id'];
        }
        $query_product = Product::query();
        if (Arr::has($data, 'product_name')) {
            $query_product->where('name', 'like', '%'.$data['product_name'].'%');
        }
        if (Arr::has($data, 'product_price_up_to')) {
            $query_product->where('price','<' ,$data['product_price_up_to']);
        }
        if (Arr::has($data, 'product_price_from')) {
            $query_product->where('price','>' ,$data['product_price_from']);
        }
        if (Arr::has($data, 'prodict_is_active')) {
            $query_product->where('isActive',$data['prodict_is_active']);
        }
        if (Arr::has($data, 'prodict_no_delete')) {
            $query_product->where('isDelete',$data['prodict_no_delete']);
        }

        $query_product= $query_product->whereIn('categories_id',$category_result)->get();
        if($query_product ->count() > 0 ){
            return response()->json($query_product);
        }
        return response()->json(['massage'=>'Список пуст']);
    }
}
