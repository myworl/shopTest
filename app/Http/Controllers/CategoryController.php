<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\Product;

class CategoryController extends Controller
{
    public function categoryAdd(CategoryRequest $request)
    {
        $category_add = Category::create([
            'name' => $request->get('name'),
        ]);
        return new CategoryResource($category_add);
    }

    public function categoryDelete($id)
    {
        $category = Category::find($id);
        $categoryProduct = Product::where('categories_id',$id)->get();
        if($categoryProduct->count() > 0){
            return response()->json(['error'=>'Категория прикреплена к товару']);
        }
        $category->delete();
        return response()->json(['message'=>'Категория удалена']);
    }
}
