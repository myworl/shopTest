<?php

namespace Database\Seeders;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 10; $i++) {
            $product[] =
                [
                    "name" => 'Товар '.$i,
                    "price" => $i*100,
                    "categories_id" => ceil($i / 2),
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ];
        }
        foreach ($product as $item) {
            Product::create($item);
        }
    }
}
