<?php

namespace Database\Seeders;

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 5; $i++) {
            $category[] =
                [
                    "name" => 'Категория '.$i,
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                ];
        }
        foreach ($category as $item) {
            Category::create($item);
        }
    }
}
