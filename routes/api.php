<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductListController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(ProductController::class)->group(function () {
    Route::post('/product/add', 'productAdd');
    Route::post('/product/edit/{id}', 'productEdit')->where(['id' => '[0-9]+']);
    Route::post('/product/delete/{id}', 'productDelete')->where(['id' => '[0-9]+']);
});

Route::controller(CategoryController::class)->group(function () {
    Route::post('/category/add', 'categoryAdd');
    Route::post('/category/delete/{id}', 'categoryDelete')->where(['id' => '[0-9]+']);
});

Route::get('/products/all', [ProductListController::class,'productsGetAll']);
